class app (
  String $app_user = 'ldap-svc-app',
  String $app_group = 'ldap-group-app',
  String $app_log_dir = "/var/log/app",
  Optional[String] $app_java = undef,
  String $app_runner_java_max_memory = '4096',
  String $app_runner_java_min_memory = '1024',
  Variant[Numeric, String] $app_memory_start = '1024',
  Variant[Numeric, String] $app_memory_max = '4096',
  Variant[Numeric, String] $app_web_port = '1080',
  # String  $app_root = '/app-default-root',
  Variant[Numeric, String] $image_import_port_import_port = '1104',
  Variant[Numeric, String] $storage_service_web_port = '1085',
  Hash $app_repo,
  Hash $app_config_repo,
  String $app_config_path = '/opt/config',
  String $app_shared_path = '/opt/shared',
  String $app_path = '/JavaPrograms/app',
  Boolean $is_app_root_mount_point = false,
  # Optional[Array] $augeas_users_xml = undef,
  # Optional[Array] $augeas_config_xml = undef,
  ){


  include profile::app

  # include java
  $app_java_real =$app_java ? {
    undef   => $::java_default_home,
    default => $app_java,
  }

  # $repo_user = $app_user

  common::mkdir_p { "${app_path}": }
  file { "${app_path}" :
    ensure  => directory,
    owner   => $app_user,
    group => $app_group,
    mode    => '2750',
    require => Common::Mkdir_p["${app_path}"];
  }

  common::mkdir_p { "${app_shared_path}": }
  file { "${app_shared_path}" :
    ensure  => directory,
    owner   => $app_user,
    group   => $app_group,
    mode    => '2750',
    require => Common::Mkdir_p["${app_shared_path}"];
  }

  common::mkdir_p { "${app_config_path}": }
  file { "${app_config_path}" :
    ensure  => directory,
    owner   => $app_user,
    group   => $app_group,
    mode    => '2750',
    require => Common::Mkdir_p["${app_config_path}"];
  }


  $app_repo_options = { # default repo options
    ensure   => latest,
    provider => 'git',
    user     => $app_user,
    group    => $app_group,
    require  => [File[$app_shared_path], File[$app_config_path]],
  }

  $app_repo_hash = {
    "$app_shared_path" => $app_repo,
    "$app_config_path" =>  $app_config_repo,
  }

  create_resources('Bitbucket_repo_app', $app_repo_hash, $app_repo_options)

  File["${app_shared_path}"] ~> File["${app_config_path}"] 
  ~> Bitbucket_repo_app["${app_shared_path}"] ~> Bitbucket_repo_app["${app_config_path}"]
  ~> Exec["changeOwnerShip"]
  ~> File["${app_path}"]
}

  if $service_provider == 'systemd' {
    file {
      '/usr/lib/systemd/system/app.service':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("app/app.service.erb");
    } ~>
    exec {
      "reload_systemctl":
        command     => "/usr/bin/systemctl daemon-reload",
        refreshonly => true;
    }
    service {
      "app":
        enable    => true,
        ensure    => running,
        subscribe =>  File['/usr/lib/systemd/system/app.service'],
    }
  } else {
    file {
      "/etc/init.d/app":
        ensure => file,
        source => "puppet:///modules/app/app",
        owner  => "root",
        group  => "root",
        mode   =>  "0755";
    }

    service {
      "app":
        enable  => true,
        ensure  => running,
        status  => "/sbin/service app status",
        restart =>   "/bin/true",
        require => [ File["/etc/sysconfig/app", "/etc/init.d/app", "$app_log_dir"] ];
    }
  }

  exec {
    'changeOwnerShip':
      command   => "/usr/bin/chown -R ${app_user}.${app::app_group} ${app_shared_path}/ ;
      /usr/bin/chown -R ${app_user}.${app::app_group} ${app_config_path}/ ";
  }

  file {
    "/etc/sysconfig/app":
      ensure  => file,
      content => template("app/app.erb"),
      owner   => "root",
      group   => "root",
      mode    => "0640",
      notify  => Service["app"];
    "$app_log_dir":
      ensure => directory,
      owner  => $app_user,
      group  => "root",
      mode   => "0750";
  }

  firewall {
    '100 app web port':
      dport   => $app_web_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
    '100 Image import port':
      dport   => $image_import_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
    '100 Storage Service Web':
      dport   => $storage_service_web_port,
      ctstate => 'NEW',
      proto   => tcp,
      action  => accept,
      chain   => 'INPUT';
  }

}
